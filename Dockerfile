# Build stage
FROM registry.gitlab.com/nevrona/public/poetry-docker:1.0.10 as builder

ARG PYTHONUNBUFFERED=1
ARG PYTHONBREAKPOINT=0
ARG BUILD_FOR_TESTS=0

WORKDIR /src/
COPY . .
# hadolint ignore=SC2046
RUN poetry export \
        --format requirements.txt \
        --output app.requirements.txt $([ "$BUILD_FOR_TESTS" = "1" ] && printf "%s" "--dev") \
    # build app
    && poetry build --format wheel


# Install app
FROM python:3.8.5-alpine3.12 AS installer

ARG PYTHONUNBUFFERED=1
ARG PYTHONBREAKPOINT=0

WORKDIR /tmp/app/
# Bring files from the build stage
COPY --from=builder /src/app.requirements.txt ./
COPY --from=builder "/src/dist/*-py3-none-any.whl" ./

# Install app
SHELL ["/bin/ash", "-eo", "pipefail", "-c"]
# hadolint ignore=DL3013
RUN pip install --no-cache-dir --requirement app.requirements.txt \
    && pip install --no-cache-dir "$(find . -type f -name "*-py3-none-any.whl")"


# Final stage
FROM python:3.8.5-alpine3.12

LABEL description="Docker image for HealthChecker"
LABEL mantainer="HacKan (https://hackan.net)"
LABEL vendor="R'lyeh Hacklab Sysadmins"
LABEL vendor.url="https://adm.rlab.be"
LABEL url="https://gitlab.com/rlyehlab/healthchecker"
LABEL version="2.1.0"
LABEL license="GNU GPL v3.0+"

ENV PYTHONUNBUFFERED 1
# Disable breakpoints (shouldn't be any but just in case)
ENV PYTHONBREAKPOINT 0

# Define limited user
ARG APP_USER=app
ARG APP_ROOT=/srv/app
ARG APP_USER_UID=2000
ARG APP_USER_GID=2000

# Create user to drop privs
RUN addgroup -g "${APP_USER_GID}" "${APP_USER}" \
    && adduser \
    -D \
    -h "${APP_ROOT}" \
    -s /sbin/nologin \
    -u "${APP_USER_UID}" \
    -G "${APP_USER}" "${APP_USER}"

# Bring site-packages from previous stage
COPY --from=installer /usr/local/lib/python3.8/site-packages /usr/local/lib/python3.8/site-packages

WORKDIR /srv/app
# Drop privs
USER "${APP_USER}"

ENTRYPOINT ["python", "-m", "healthchecker"]

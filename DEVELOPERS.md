# Developers

This section is intended for the use of this app as a python module.

## Making PRs

For a pull request of whatever change you want to do, the following commands must succeed locally:

* `inv reformat`: format code using YAPF.
* `inv lint`: static analysis for PEP8, PEP257 and PEP287 compliance.
* `inv tests`: run the tests battery.
* `inv safety`: run a security analysis over dependencies using `safety`.

If the linter complains about *code too complex*, run `inv cc -c` (or the long expression `inv cyclomatic-complexity --complex`) for more information.

If you add or change some functionality, your PR must include the necessary docstrings and unit tests so that coverage remains 100%.

If you create code then add the appropriate license header.

## Public API

Currently, the following functions are exposed:

* `healthcheck_url`: Check if a URL is alive or not. Return a `ServiceStatus` object.
* `healthcheck_urls`: Asynchronously check given URLs status, optionally validating them. Return a `ServiceStatusList` object.
* `notify`: Execute a POST request as a notification with optional data. Return a boolean representing the result of the request.

And the following classes:

* `ServiceStatus`: Container for the result of a health check.
* `ServiceStatusList`: List of `ServiceStatus` objects. It is used just like any other Python list.

## Examples

### Check a single URL without logs

```python
from healthchecker import healthcheck_url


my_url = 'https://blog.adm.rlab.be'
result = healthcheck_url(my_url, timeout=5)  # No logs

# `result` is a ServiceStatus object that has the URL and its status in two
# properties: alive and ok, where the first one means that there was some
# answer from the service (statuses between 2xx and 4xx, note that 3xx are
# automatically followed by the requests library used in the backend) and ok
# means the status was 2xx and if there were some verifications applied to
# the body of the request, then the result of those verifications as passed
# or failed. 
print(result)  # https://blog.adm.rlab.be is ALIVE and OK
if result.ok:
    print('Great!')
elif result.alive:
    print('Well, the service responded but something went wrong')

# Example of alive but not OK:
print(healthcheck_url('https://hackan.net', timeout=5)) # https://hackan.net is ALIVE but NOT OK
# Because CloudFlare blocks the request and asks for some sort of JS validation.
```

### Check several URLs and show logs

Note that the logs are handled by standard Python logger and thus its verbosity is determined by the configuration applied.

```python
from healthchecker import healthcheck_urls


my_urls = 'https://antifa-glug.org', 'https://caba.flisol.org.ar'
# Let's play a bit and make them time out
result = await healthcheck_urls(my_urls, timeout=0.1, log=True)
# You might see logs like the following:

# Request to https://blog.adm.rlab.be took too long: 0.20 seconds
# Error GETing data from/to https://caba.flisol.org.ar: ReadTimeout(ReadTimeoutError("HTTPSConnectionPool(host='eventol.flisol.org.ar', port=443): Read timed out. (read timeout=0.1)"))
# Request to https://caba.flisol.org.ar timed out taking 0.41 seconds

# `result` is a ServiceStatusList object, which behaves exactly like a
# Python list except it carries only ServiceStatus objects.
print(result)  # https://antifa-glug.org is ALIVE and OK, https://caba.flisol.org.ar is DEAD
```

### Send a notification

The CLI sends notifications when some service fails, by POSTing the failed service data to the given endpoint.  
Again, this function can display logs or not using the `log` parameter.

```python
import requests
from healthchecker import healthcheck_url, notify


status = healthcheck_url('https://hackan.net', timeout=5)
notified = notify('https://www.hashemian.com/tools/form-post-tester.php/healthchecker', f'Developers sample: {status}')
# `notify` returns True if the POST was successful, False otherwise
# You can see the result, if the POST was successful, at: https://www.hashemian.com/tools/form-post-tester.php?code=healthchecker&b=View
if notified:
    print(requests.get('https://www.hashemian.com/tools/form-post-tester.php?code=healthchecker&b=View').text)
    # The POSTed value is:
    # ********************
    # Developers sample: https://hackan.net is ALIVE but NOT OK
    # ********************
    #
```

### More parameters!

The amazing [requests](https://2.python-requests.org/) library is used at the back and to keep its flexibility it is exposed by core functions. So just `healthcheck_url(my_url, timeout=5, param_for_requests=True, another_param_for_requests='foo')`, and the same for any other core function.

